<?php namespace Qualitare\Drnahora\Updates\Seeds;

use October\Rain\Database\Updates\Seeder;
use Qualitare\Drnahora\Models\Unidades;
use Faker;

class UnidadesTableSeeder extends Seeder
{

	public function run()
	{
		$faker = Faker\Factory::create();

		for ($i = 1; $i <= 3; $i++) {
			for ($k = 0; $k < 10; $k++) {
				$unidade = Unidades::create([
					'name' => $faker->sentence(3, true),
					'endereco' => $faker->address(),
					'eixo_x' => $faker->latitude(-180, 180),
					'eixo_y' => $faker->longitude(-180, 180),
                    'telefone' => $faker->phoneNumber(),
                    'cidade' => $faker->randomElement(['João Pessoa','Campina Grande']),
					'horario_atendimento' => $faker->text(50),
				]);

                $unidade->save();
			}
		}
	}
}
