<?php namespace Qualitare\Drnahora\Updates\Seeds;

use October\Rain\Database\Updates\Seeder;
use Qualitare\Drnahora\Models\SejaMedico;
use Faker;

class SejaMedicoTableSeeder extends Seeder
{

	public function run()
	{
		$faker = Faker\Factory::create();

		for ($i = 1; $i <= 3; $i++) {
			for ($k = 0; $k < 10; $k++) {
				$medico = SejaMedico::create([
					'nome' => $faker->sentence(3, true),
					'email' => $faker->email(),
					'telefone' => $faker->phoneNumber(),
                    'mensagem' => $faker->text(100),
				]);

                $medico->save();
			}
		}
	}
}
