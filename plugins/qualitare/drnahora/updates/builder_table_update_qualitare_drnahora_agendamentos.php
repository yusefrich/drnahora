<?php namespace Qualitare\Drnahora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateeQualitareDrnahoraAgendamentos extends Migration
{
    public function up()
    {
        Schema::table('qualitare_drnahora_agendamentos', function($table)
        {
            $table->string('phone')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('qualitare_drnahora_agendamentos', function($table)
        {
            $table->dropColumn('phone');
        });
    }
}
