<?php namespace Qualitare\Drnahora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddedUsersSocialFields extends Migration
{
	public function up()
	{
		Schema::table('users', function($table)
		{
            $table->text('google_id')->nullable();
            $table->text('google_token')->nullable();

            $table->text('facebook_id')->nullable();
            $table->text('facebook_token')->nullable();
		});
	}

	public function down()
	{
		Schema::table('users', function($table)
		{
			$table->dropColumn('google_id');
			$table->dropColumn('google_token');

            $table->dropColumn('facebook_id');
            $table->dropColumn('facebook_token');
		});
	}
}
