<?php namespace Qualitare\Drnahora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareDrnahoraContadores extends Migration
{
    public function up()
    {
        Schema::create('qualitare_drnahora_contadores', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nome', 40);
            $table->integer('quantidade');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_drnahora_contadores');
    }
}
