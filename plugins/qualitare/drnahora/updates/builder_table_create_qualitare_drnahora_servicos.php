<?php namespace Qualitare\Drnahora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareDrnahoraServicos extends Migration
{
    public function up()
    {
        Schema::create('qualitare_drnahora_servicos', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nome', 200);
            $table->string('tipo', 1);
            $table->integer('shosp_id');
            $table->string('categoria', 30)->nullable();
            $table->text('orientacoes_gerais')->nullable();
            $table->text('restricoes')->nullable();
            $table->text('sobre_procedimento')->nullable();
            $table->text('valor');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_drnahora_servicos');
    }
}
