<?php namespace Qualitare\Drnahora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareDrnahoraAgendamentos extends Migration
{
    public function up()
    {
        Schema::create('qualitare_drnahora_agendamentos', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('medico_id');
            $table->integer('servico_id');
            $table->integer('codigo_shosp');
            $table->date('data');
            $table->time('horario');
            $table->string('status', 1);
            $table->integer('desmarcou');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_drnahora_agendamentos');
    }
}
