<?php namespace Qualitare\DrNaHora\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateEspecialidadesTable extends Migration
{
    public function up()
    {
        Schema::create('qualitare_drnahora_especialidades', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
						$table->string('name');
						$table->integer('codigo_shosp');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('qualitare_drnahora_especialidades');
    }
}
