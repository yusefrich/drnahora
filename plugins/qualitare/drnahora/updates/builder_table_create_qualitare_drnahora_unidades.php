<?php namespace Qualitare\Drnahora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareDrnahoraUnidades extends Migration
{
    public function up()
    {
        Schema::create('qualitare_drnahora_unidades', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('codigo_shosp');
            $table->string('name', 100);
            $table->string('endereco', 250);
            $table->string('eixo_x', 150);
            $table->string('eixo_y', 150);
            $table->string('telefone', 150)->nullable();;
            $table->string('cidade', 20)->nullable();;
            $table->string('horario_atendimento', 100);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_drnahora_unidades');
    }
}
