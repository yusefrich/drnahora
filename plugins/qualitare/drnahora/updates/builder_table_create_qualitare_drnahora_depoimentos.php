<?php namespace Qualitare\Drnahora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareDrnahoraDepoimentos extends Migration
{
    public function up()
    {
        Schema::create('qualitare_drnahora_depoimentos', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nome', 100);
            $table->string('idade', 2)->nullable();
            $table->string('profissao', 150)->nullable();
            $table->text('depoimento');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_drnahora_depoimentos');
    }
}
