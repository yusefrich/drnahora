<?php namespace Qualitare\Drnahora\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareDrnahoraMedicos extends Migration
{
    public function up()
    {
        Schema::create('qualitare_drnahora_medicos', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 200);
            $table->integer('codigo_shosp');
            $table->text('formacao');
            $table->text('residencia');
            $table->string('orgao', 20)->nullable();
            $table->string('codigo_orgao', 40)->nullable();
            $table->string('uf_orgao', 20)->nullable();
            $table->text('especialidades');
            $table->smallInteger('todas_idades')->default(0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_drnahora_medicos');
    }
}
