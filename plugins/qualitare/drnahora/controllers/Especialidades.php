<?php namespace Qualitare\DrNaHora\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Qualitare\Drnahora\Models\Especialidade;
use Qualitare\Drnahora\Models\Servicos;

/**
 * Especialidades Back-end Controller
 */
class Especialidades extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Qualitare.DrNaHora', 'drnahora', 'especialidades');
    }

    public function especialities()
    {
        $especialidadeModel = new Especialidade();

        $data = Servicos::with('especialidade')->where('tipo', 'C')->get(['id', 'nome', 'especialidade_id']);
        $data = $data->toArray();
        $i = 0;
        foreach ($data as $row) {
            $data[$i]['medicos'] = $especialidadeModel->getMedicosByEspecialidade($row['especialidade_id'])->first();
            unset($data[$i]['especialidade_id']);
            $i++;
        }
        return response()->json($data);
    }
}
