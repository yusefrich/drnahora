<?php

namespace Qualitare\DrNaHora\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Backend\Classes\Controller;

class SitemapController extends Controller
{
    public function __construct()
    {

    }
    /**
    * Display sitemap.
    * @return Response
    */
    public function index()
    {

    $sitemap = app()->make('sitemap');

    // Check if theere is cached sitemap and build new only if is not
    // if ($sitemap->isCached()):

        // Add home to the sitemap
       $sitemap->add('http://drnahora.com.br', '2018-05-28T20:10:00+02:00', '1.0', 'daily');

        // Add about to the consultas
       $sitemap->add('http://drnahora.com.br/consultas', '2018-05-28T20:10:00+02:00', '1.0', 'daily');

        // Add about to the exames
       $sitemap->add('http://drnahora.com.br/exames', '2018-05-28T20:10:00+02:00', '1.0', 'daily');

        // Add about to the medicos
       $sitemap->add('http://drnahora.com.br/medicos', '2018-05-28T20:10:00+02:00', '1.0', 'daily');

        // Add about to the inscricao
       $sitemap->add('http://drnahora.com.br/inscricao', '2018-05-28T20:10:00+02:00', '1.0', 'daily');

        // Add about to the blog
       $sitemap->add('http://drnahora.com.br/blog', '2018-05-28T20:10:00+02:00', '1.0', 'daily');

        // Add about to the contato
       $sitemap->add('http://drnahora.com.br/contato', '2018-05-28T20:10:00+02:00', '1.0', 'daily');

        // Add about to the login
       $sitemap->add('http://drnahora.com.br/auth/login', '2018-05-28T20:10:00+02:00', '1.0', 'daily');
    // endif;

    // Return sitemap
    return $sitemap->render('xml');
  }
}
