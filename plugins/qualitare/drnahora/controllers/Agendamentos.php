<?php namespace Qualitare\Drnahora\Controllers;

use Backend\Classes\Controller;
use Backend\Models\User;
use BackendMenu;
use Hybridauth\Exception\Exception;
use Qualitare\Drnahora\Models\Agendamento;
use Qualitare\Drnahora\Models\Payment;
use Illuminate\Http\Request;
use Qualitare\Drnahora\Models\Paciente;
use Qualitare\Drnahora\Models\Servicos;
use Qualitare\Drnahora\Models\Medicos;
use Qualitare\Drnahora\Models\Unidades;
use Qualitare\Drnahora\Models\Especialidade;
use Qualitare\Drnahora\Models\Shosp;
use RainLab\User\Models\User as ModelsUser;

class Agendamentos extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\ReorderController',
        'Backend.Behaviors.ImportExportController'
    ];

    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';
    public $importExportConfig = 'config_import_export.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Qualitare.Drnahora', 'main-menu-item7');
    }

    public function create(Request $request)
    {
        $input = $request->all();
        // Check donation type
        if ($input['type'] == 'pessoa-fisica'){
            $pessoa = $input['donation-pessoa'];
            $document = $input['donation-cpf'];
        }else{
            $pessoa = $input['donation-empresa'];
            $document = $input['donation-cnpj'];
        }
        // Donation data
        $data = [
            'name' => $pessoa,
            'document' => $document,
            'type' => $input['type'],
            'value'  => $input['value'],
            'monthly'  => $input['monthly']
        ];

        // Create donation and session
        $donation = Donation::create($data);
        $request->session()->put('donation', $donation->id);

        // Return created donation
        return response([
            'donation' => $donation
        ], 200);
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $user = ModelsUser::where('email', $input['email'])->first();
        if (! $user) {
            $user = ModelsUser::create([
                'name' => $input['nome'],
                'email' => $input['email'],
                'password' => 'drnahora123456',
                'password_confirmation' => 'drnahora123456',
            ]);
        }
        $pacient = Paciente::where('user_id', $user->id)->first();
        if ($pacient) {
            $id = $pacient->id;
        } else {
            $response = (new Shosp())->createPaciente(
                $input['nome'],
                $input['email'],
                $input['telefone'],
                $input['telefone']
            );
            $id = $response['dados']['prontuario'];

            $pacient = Paciente::create([
                'user_id' => $user->id,
                'nome' => $input['nome'],
                'codigo_shosp' => $id
            ]);
        }

        $arrayAgendamento = [
            'codigoPrestador' => $input['codigoPrestador'],
            'codigoUnidade' => $input['codigoUnidade'],
            'codigoServico' => $input['codigoServico'],
            'data' => $input['data'],
            'horario' => $input['horario'],
            'codigoHorario' => $input['codigoHorario'],
            'codigoPaciente' => $pacient->codigo_shosp,
            'codigoPlanoSaude' => 1,
            'nome' => $pacient->nome,
            'telefone' => $input['telefone'],
            'email' => $input['email'],
            'dataNascimento' => $input['dataNascimento'],
            'sexo' => $input['sexo'],
        ];

        $response = (new Shosp())->createAgendamento($arrayAgendamento);
        if ($response != false) {
            $servico = Servicos::where('shosp_id', $input['codigoServico'])->first();
            $medico = Medicos::where('codigo_shosp', $input['codigoPrestador'])->first();
            $unidade = Unidades::where('codigo_shosp', $input['codigoUnidade'])->first();

            $agendamento = Agendamento::create([
                'user_id' => $user->id,
                'paciente_id' => $pacient->id,
                'medico_id' => $medico->id,
                'servico_id' => $servico->id,
                'unidade_id' => $unidade->id,
                'horario' => $input['horario'],
                'horario_codigo' => $input['codigoHorario'],
                'phone' => $input['telefone'],
                'data' => $input['data'],
                'status' => 'p',
                'desmarcou' => 0,
                'codigo_shosp' => $response['codigoAgendamento']
            ]);
        }

        return response()->json(['data' => $response], 201);
    }

    public function schedules($especiality)
    {
        $start = date('Y-m-d');
        $s = Servicos::find($especiality);
        $data = Agendamento::getAvailableDates($start, $s->especialidade->codigo_shosp, null, false, $s);

        return response()->json($data, 201);
    }

    public function user(Request $request)
    {
        $input = $request->all();

        $user = ModelsUser::where('email', $input['email'])->first();
        $response = [];
        if (!empty($user->id)) {
            $agendamentos = Agendamento::where('user_id', $user->id);
            if (isset($input['old']) && $input['old'] == true) {
                $date = date('y-m-d', strtotime('-15 days'));
                $agendamentos = $agendamentos->whereDate('data', '>=', $date)
                                             ->whereDate('data', '<', date('Y-m-d'));
            } else {
                $agendamentos = $agendamentos->whereDate('data', '>', date('Y-m-d'));
            }
            $agendamentos = $agendamentos->get();

            foreach ($agendamentos as $ag) {
                $servico = Servicos::find($ag->servico_id);
                $medico = Medicos::find($ag->medico_id);
                $unidade = Unidades::find($ag->unidade_id);

                if (!$servico || !$medico)
                    continue;

                $response[] = [
                    'id' => $ag->id,
                    'medico' => $medico->name,
                    'servico' => $servico->nome,
                    'servico_id' => $servico->id,
                    'shosp_id' => $ag->codigo_shosp,
                    'status' => $ag->status,
                    'data' => $ag->data,
                    'horario' => $ag->horario,
                    'desmarcado' => $ag->desmarcou,
                    'endereco' => $unidade->endereco,
                    'lat' => $unidade->eixo_x,
                    'lng' => $unidade->eixo_y
                ];
            }
        }

        return response()->json(['data' => $response], 201);
    }

    public function destroy(Request $request)
    {
        $input = $request->all();

        $agendamento = Agendamento::where('codigo_shosp', $input['id'])->first();

        (new Shosp())->cancelarAgendamento($agendamento->codigo_shosp);

        $agendamento->delete();

        return response()->json($agendamento, 201);
    }

    public function checkout(Request $request) {
        $input = $request->all();
        $pagseguro = new Payment();

        dd($pagseguro->getCheckout());
    }

    public function notification(Request $request) {
        $input = $request->all();

        try {
            // Initialize PagSeguro
            \PagSeguro\Library::initialize();
            \PagSeguro\Configuration\Configure::setEnvironment(env('PAGSEGURO_ENV', 'sandbox'));

            $credentials = new \PagSeguro\Domains\AccountCredentials(env('PAGSEGURO_EMAIL'), env('PAGSEGURO_TOKEN'));
            $response = \PagSeguro\Services\Transactions\Notification::check($credentials);

            if (filter_var($response->getReference(), FILTER_VALIDATE_INT) === false)
                return response('success', 200);

            $donation = Donation::find($response->getReference());
            $donation->email = $response->getSender()->getEmail();

            if ($response->getStatus() == 3 || $response->getStatus == 4)
                $donation->code = $response->getCode();

            $donation->save();

            // Notifies customer
            $donation->sendDonationNotification();

        } catch (Exception $e) {
            die($e->getMessage());
        }

        return response('success', 200);
    }

    public function tofloat($num)
    {
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
            ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

        if (!$sep) {
            return floatval(preg_replace("/[^0-9]/", "", $num));
        }

        return floatval(
            preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
            preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
        );
    }
}
