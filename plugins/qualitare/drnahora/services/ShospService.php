<?php namespace Qualitare\Api\Services;

use Qualitare\Api\Services\Video;
use Qualitare\Tv\Models\Show;
use Madcoda\Youtube\Youtube;

class ShospService
{
	private $api;
	public $key;

	public function __construct($key, $channel)
	{
		$this->youtube = new Youtube(['key' => $key]);
		$this->channel = $channel;
	}

	/*
	 * Get a single video
	 *
	 * @param string $id - The youtube video id
	 * @return Video
	 */
	public function get($id) {
		$response = $this->youtube->getVideoInfo($id);
		$video = Video::withResponse($response);

		return $video;
	}

	/*
	 * Search videos from the channel
	 *
	 * @param string $keyword - The query to search videos
	 * @return array Video
	 */
	public function search($keyword) {
		$videos = [];
		$response = $this->youtube->searchChannelVideos($keyword, $this->channel);

		if ($response) {
			$filtered = $this->filter($response);
			$videos = $this->transform($filtered);
		}

		return $videos;
	}

	/*
	 * Get all videos from a playlist
	 *
	 * @param string $playlist - The playlist id
	 * @return array Video
	 */
	public function list($playlist) {
		$videos = [];
		$args = [
			'playlistId' => $playlist,
			'part' => 'snippet',
			'maxResults' => 50,
			'pageToken' => ''
		];

		// Get content from all pages
		while ($args['pageToken'] !== null) {
			$response = $this->youtube->getPlaylistItemsByPlaylistIdAdvanced($args, true);
			$filtered = $this->filter($response['results']);
			$updated = $this->transform($filtered);
			$videos = array_merge($videos, $updated);

			$args['pageToken'] = $response['info']['nextPageToken'];
		}

		return $videos;
	}

	/*
	 * Convert response data into an array of Video objects
	 *
	 * @param array $items - Youtube videos response
	 * @return array Video
	 */
	public function filter($items) {
		$videos = [];

		foreach ($items as $item) {
			$video = Video::withResponse($item);

			// Ignore if video is private
			if ($video->title != 'Private video')
				$videos[$video->id] = $video;
		}

		return $videos;
	}

	/*
	 * Append duration, like and views into a array of Video
	 *
	 * @param array $items - List of Video objects
	 * @return array Video
	 */
	public function transform($videos) {
		// Send a request to get extra video data
		$extra = $this->youtube->getVideosInfo(
			implode(',', array_map(function($video) {
				return $video->id;
			}, $videos))
		);

		// Update videos extra content
		foreach ($extra as $data) {
			$statistics = $data->statistics;
			$details = $data->contentDetails;

			$videos[$data->id]->setDuration($details->duration);

			if (isset($statistics->likeCount))
				$videos[$data->id]->likes = $statistics->likeCount;

			if (isset($statistics->viewCount))
				$videos[$data->id]->views = $statistics->viewCount;
		}

		return array_values($videos);
	}
}
