<?php namespace Qualitare\Api\Providers;

use October\Rain\Support\ServiceProvider;
use Qualitare\Api\Services\ShospService;

class ShospServiceProvider extends ServiceProvider
{
	public function register()
	{
		$this->app->bind('ShospService', function() {
			return new ShospService(env('SHOSP_API_ID'), env('SHOSP_API_KEY'));
		});
	}
}
