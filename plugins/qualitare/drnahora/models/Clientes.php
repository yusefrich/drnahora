<?php namespace Qualitare\Drnahora\Models;

use Model;

/**
 * Model
 */
class Clientes extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_drnahora_clientes';
    public $fillable = ['nome', 'email', 'telefone', 'senha', 'cpf', 'codigo_shosp'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];


}
