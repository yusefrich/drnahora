<?php namespace Qualitare\Drnahora\Models;

use GuzzleHttp\Client;

/**
 * Model
 */
class Payment
{

    private $client;
    private $headers = [
        'Accept' => 'application/json',
        'id' => '',
        'x-api-key' => ''
    ];

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://ws.sandbox.pagseguro.uol.com.br/v2/checkout',
        ]);
    }

    public function getCheckout(){
        $params = [
            'email' => 'p4mpah@gmail.com',
            'token' => env('PAGSEGURO_TOKEN'),
            'currency' => 'BRL',
            'itemId1' => '1',
            'itemDescription1' => 'Produto PagSeguro',
            'itemAmount1' => 9.99,
            'itemQuantity1' => 1,
            'itemWeight1' => 1000,
            'reference' => 'REF1234',
            'senderName' => 'Jose Comprador',
            'senderAreaCode' => 83,
            'senderPhone' => '996325857',
            'senderEmail' => 'jonathanbeltrao@gmail.com',
            'timeout' => 120,
        ];
        try{
            $response = $this->client->request(
                'POST',
                '',
                [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'charset' => 'ISO-8859-1',
                    'form_params' => $params
                ]
            );
            $data = json_decode($response->getBody()->getContents(), true);
            dd($data);

            return $data['horarios'][$params['dataInicial']]['horario'];

        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

}
