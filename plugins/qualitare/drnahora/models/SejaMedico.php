<?php namespace Qualitare\Drnahora\Models;

use Model;

/**
 * Model
 */
class SejaMedico extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_drnahora_seja_medico';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
