<?php namespace Qualitare\Drnahora\Models;

use Model;
use Qualitare\Drnahora\Models\Shosp;
use Qualitare\Drnahora\Models\Medicos;

/**
 * Model
 */
class Servicos extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_drnahora_servicos';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $jsonable = ['medicos'];
    public $with = [ 'medicos', 'especialidade'];

    public $belongsToMany = [
        'medicos' => [
            'Qualitare\Drnahora\Models\Medicos',
            'table' => 'qualitare_drnahora_servico_medicos'
        ]
    ];

    public $belongsTo = [
        'especialidade' => 'Qualitare\Drnahora\Models\Especialidade'
    ];

    public function beforeSave()
    {
        $shosp = new Shosp();
        foreach ($shosp->getServicos() as $s) {
            if ($s['codigoServico'] == $this->shosp_id) {
                $this->tipo = $s['tipo'];
                $this->valor = $s['valor'];
            }
        }
    }

    public function beforeUpdate()
    {
        $shosp = new Shosp();
        foreach ($shosp->getServicos() as $s) {
            if ($s['codigoServico'] == $this->shosp_id) {
                $this->valor = $s['valor'];
            }
        }
    }

    public function getShospIdOptions()
    {
        $shosp = new Shosp();
        $servicos = [];

        $records = $shosp->getServicos();

        if ($records) {
            foreach ($records as $s) {
                switch ($s['tipo']) {
                    case "E":
                        $servicos[$s['codigoServico']] = $s['nome'] . "  [E]";
                        break;
                    case "C":
                        $servicos[$s['codigoServico']] = $s['nome'] . " [C]";
                        break;
                }
            }
        }
        return $servicos;
    }

    public function getMedicosOptions()
    {
        $medico = new Medicos();
        $medicos = [];
        foreach ($medico->all() as $m) {
            $medicos[$m->id] = $m->name;
        }
        return $medicos;
    }

    public function getCategoriaOptions()
    {
        return [
            '' => '',
            'laboratoriais' => 'Exames Laboratoriais',
            'imagem' => 'Exames de Imagem',
            'coracao' => 'Exames do Coração',
        ];
    }

}
