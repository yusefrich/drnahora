<?php namespace Qualitare\Drnahora\Models;

use Model;
use Qualitare\Drnahora\Models\Shosp;
use Qualitare\Drnahora\Models\Unidades;

/**
 * Model
 */
class Medicos extends Model
{
	use \October\Rain\Database\Traits\Validation;

	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'qualitare_drnahora_medicos';

	/**
	 * @var array Validation rules
	 */
	public $rules = [
	];

	public $with = ['foto', 'curriculo'];

	public $attachOne = [
		'foto' => 'System\Models\File',
        'curriculo' => 'System\Models\File'
	];

	protected $casts = [
		'especialidades_shosp' => 'array',
	];

	public $jsonable = ['especialidades'];

	public $belongsToMany = [
		'especialidades' => [
			'Qualitare\Drnahora\Models\Especialidade',
			'table' => 'qualitare_drnahora_especialidade_medico'
		],
		'unidades' => [
			'Qualitare\Drnahora\Models\Unidades',
			'table' => 'qualitare_drnahora_medico_unidades'
		],

	];

	public function beforeCreate(){
		$shosp = new Shosp();
		$medico = $shosp->getMedicos(null, $this->codigo_shosp);
		$this->especialidades_shosp = $medico[0]['especialidades'];

		if (isset($medico[0]['conselhoprofissional']))
			$this->orgao = $medico[0]['conselhoprofissional'];
		if (isset($medico[0]['ufconselho']))
			$this->uf_orgao = $medico[0]['ufconselho'];
		if (isset($medico[0]['numeroConselhoClasse']))
			$this->codigo_orgao = $medico[0]['numeroConselhoClasse'];
	}

	public function afterSave() {

  
		/* Get especialidades
		$especialidades = Especialidade::whereIn(
			'codigo_shosp',
			array_map(
				function($especialidade) {
					return $especialidade['codigoEspecialidade'];
				},
				$this->especialidades_shosp
			)
		)->get();

		// Transform into array of ids
		$this->especialidades()
			->attach(
				$especialidades->map(function($especialidade) {
					return $especialidade->id;
				})
			);
		 */
	}

	public function getCodigoShospOptions(){
		$shosp = new Shosp();
		$servicos = [];
		foreach($shosp->getMedicos() as $m){
			$servicos[$m['codigo']] =  $m['nome'];
		}

		return $servicos;
	}

	public function getUnidadesOptions(){
		$unidade = new Unidades();
		$unidades = [];
		foreach($unidade->all() as $u){
			$unidades[$u['id']] =  $u['nome'];
		}

		return $unidades;
	}

/*	public function getAttributeCurriculo($value)
    {
        return base
    }*/
}
