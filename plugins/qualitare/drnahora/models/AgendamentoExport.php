<?php namespace Qualitare\Drnahora\Models;

use Model;
use Qualitare\Drnahora\Models\Shosp;

/**
 * Model
 */
class AgendamentoExport extends \Backend\Models\ExportModel {

    public function exportData($columns, $sessionKey = null) {
        $subscribers = Agendamento::all();
        $subscribers->each(function($subscriber) use ($columns) {
            $subscriber->addVisible($columns);
        });
        return $subscribers->toArray();
    }

}
