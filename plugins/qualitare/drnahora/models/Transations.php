<?php namespace Qualitare\Drnahora\Models;

use Model;

/**
 * Transations Model
 */
class Transations extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_drnahora_transations';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'transaction_code',
        'body',
        'payment_status',
        'notification',
        'installments',
        'payment_method_type',
        'payment_method_code',
    ];

    protected $casts = [
        'body' => 'array',
        'notification' => 'array',
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [
        'payable' => []
    ];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
