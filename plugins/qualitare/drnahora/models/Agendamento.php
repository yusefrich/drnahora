<?php namespace Qualitare\Drnahora\Models;

use Model;
use Qualitare\Api\Services\PagseguroService;
use Qualitare\Drnahora\Models\Unidades;
use Qualitare\Drnahora\Models\Medicos;
use Qualitare\Drnahora\Models\Especialidade;
use Rainlab\User\Models\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Session;

/**
 * Model
 */
class Agendamento extends Model
{
	use \October\Rain\Database\Traits\Validation;
	use \October\Rain\Database\Traits\SoftDelete;

	protected $dates = ['deleted_at'];

	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'qualitare_drnahora_agendamentos';

	/**
	 * @var array Validation rules
	 */
	public $rules = [
	];

    public $hasOne = [
        'paciente' => 'Qualitare\Drnahora\Models\Paciente'
    ];

	public $belongsTo = [
		'user' => 'RainLab\User\Models\User',
		'servico' => 'Qualitare\Drnahora\Models\Servicos',
		'medico' => 'Qualitare\Drnahora\Models\Medicos',
		'unidade' => 'Qualitare\Drnahora\Models\Unidades'
	];

	public $fillable = [
		'user_id', 'paciente_id', 'medico_id', 'servico_id', 'unidade_id', 'codigo_shosp', 'horario', 'horario_codigo', 'data', 'status', 'phone', 'desmarcou'
	];

    public static function createPeriod($start, $interval, $s)
    {
		$date = Carbon::createFromFormat('Y-m-d', $start);
        $end = $date->copy()->endOfMonth();
        if ($s) {
            $end = $date->copy()->addDays('15');
        }
		$period = CarbonPeriod::create($date, $interval . ' days', $end);

		return $period;
	}

	public static function getShift($time) {
		$date = Carbon::createFromFormat("H:i", $time);
		$hour = $date->hour;

		if ($hour < "12") {
			return 'morning';
		} else if ($hour >= "12" && $hour < "18") {
			return 'afternoon';
		} else if ($hour >= "18") {
			return 'night';
		}
	}

	public static function splitTimes($times, $shift) {
		$chunk = [];

		$cont = 1;

		foreach ($times as $time) {
			if ( self::getShift($time) == $shift && ($cont < 4) ) {
				$chunk[] = $time;
			}
			$cont++;
		}

		return $chunk;
	}

// 1. Get first unit
// 2. Get speciality code
// 3. Get current date as initial
// 4. Make the first request to SHOSP
// 5. Populate data following the example below
// 6. Add 7 days from the previous date to get the next current one
// 7. Repeat the steps 3, 4, 5 and 6 until reach the end of moth
// 8. Get the next unit
// 9. Reset the initial date
// 10. Repeat the steps 3, 4, 5 and 6
// 11. Repeat the 8 step until there's no more unities

// Example:
// {
// 	'2019-02-01': [
// 		{
// 			codigo: 72525,
// 			unidade: {...},
// 			medico: {...},
// 			morning: ['06:30', '07:00'],
// 			afternoon: ['15:00', '15:30'],
// 			night: ['18:30', '19:00']
// 		},
//
// 		...
//
// 	],
//
// 	...
//
// }
	public static function getAvailableDates($start, $especialidade_shosp, $medico_shosp = null, $online = false, $servico = null)
	{
		$data = [];

		$shosp = new Shosp();
		$unities = $online ? ['3'] : ['1', '2', '4'];
        $s = $servico ?: null;
        //$periods = self::createPeriod($start, $interval, $s);
		$interval = $s ? 15 : 30;
		// Loop through unities
		foreach ($unities as $unit) {
			// Loop through periods
			//foreach ($periods as $date) {
				$available = [];

				// Get availability starting
				$response = $shosp->getAgenda(
					date('Y-m-d'),
					$interval,
					$unit,
					$especialidade_shosp,
					$medico_shosp
                );

                if (!isset($response['dados'][0])) {
                    return [];
                }
				// Get especialistas from response
				$especialistas = $response['dados'][0];

				if ( $especialistas ) {

                    foreach ($especialistas as $especialista) {
                        if (!isset($especialista['horarios']))
                            continue;

                        foreach ($especialista['horarios'] as $day => $current) {
                            $times = [];
                            $codigo = null;
                            $codigo_shosp = isset($especialista['codigo']) ?
                                $especialista['codigo'] :
                                $especialista['codigoPrestador'];
                            $doctor = Medicos::where('codigo_shosp', $codigo_shosp)->first();
                            $schedules = $current['horario'];

                            // Skip if we cant find the doctor
                            if (!$doctor)
                                continue;

                            // Create empty day block if it is not empty
                            if (count($schedules) > 0 && !isset($data[$day]))
                                $data[$day] = [];

                            // Continue if there are no times
                            if (count($schedules) == 0)
                                continue;

                            // Loop through available times
                            foreach ($schedules as $time) {
                                // Update time code
                                if (isset($time['codigoHorario'])) {
                                    $times[] = $time['horario'];
                                    $codigo = $time['codigoHorario'];
                                }
                            }

                            // Continue if we cant find the time code
                            if (!$codigo)
                                continue;

                            // Populate data with the current registry
                            $data[$day][] = [
                                'codigo' => $codigo,
                                'unidade' => Unidades::where('codigo_shosp', $especialista['codigoUnidade'])->first(),
                                'medico' => $doctor,
                                'servico' => $s,
                                'morning' => self::splitTimes($times, 'morning'),
                                'afternoon' => self::splitTimes($times, 'afternoon'),
                                'night' => self::splitTimes($times, 'night')
                            ];
                        }
                    }

                }


			//}
		}
		return array_filter($data);
	}

	public function checkout() {
		$usuario = $this->user;
		$servico = $this->servico;

		// Initialize PagSeguro
		\PagSeguro\Library::initialize();

		// Set environment to sandbox or production
		\PagSeguro\Configuration\Configure::setEnvironment(env('PAGSEGURO_ENVIRONMENT', 'sandbox'));

		// If seller profile flow
		\PagSeguro\Configuration\Configure::setAccountCredentials(
			env('PAGSEGURO_EMAIL'), env('PAGSEGURO_TOKEN')
		);

		// Create payment
		$payment = new \PagSeguro\Domains\Requests\Payment();

		$payment->addItems()->withParameters(
			'0001',
			$servico->nome,
			1,
			$servico->valor
		);
		$payment->setCurrency("BRL");
		$payment->setReference($this->id);
		$payment->setRedirectUrl(env('APP_URL'));
		$payment->setExtraAmount(- ((float)$servico->valor) * 0.1);

		// Set your customer information.
//		$payment->setSender()->setName($usuario->name);
		$payment->setSender()->setEmail($usuario->email);

		$payment->setShipping()->setType()->withParameters(
			\PagSeguro\Enum\Shipping\Type::NOT_SPECIFIED
		);
		$payment->setShipping()->setAddress()->withParameters(
			'Rua João Alves de Oliveira',
			'189',
			'Centro',
			'58400-117',
			'Campina Grande',
			'PB',
			'BRA',
			''
		);

		try {
			$result = $payment->register(
				\PagSeguro\Configuration\Configure::getAccountCredentials(),
				true
			);

			return response(['checkout_code' => $result->getCode()], 200);
		} catch (Exception $e) {
			return response(['error' => $e->getMessage()], 400);
		}

        /*$data = [
            'reference' => '$this->id',
            'extra_amount' => (-((float)$servico->valor) * 0.1),
            'items' => [
                [
                    'id' => '0001',
                    'description' => $servico->nome,
                    'quantity' => 1,
                    'amount' => $servico->valor
                ]
            ],
            'sender' => [
                'name' => $usuario->name,
                'email' => $usuario->email
            ],
            'shipping' => [
                'address' => [
                    'street' => 'Rua João Alves de Oliveira',
                    'number' => '189',
                    'district' => 'Centro',
                    'postalCode' => '58400-117',
                    'city' => 'Campina Grande',
                    'state' => 'PB',
                    'country' => 'BRA'
                ]
            ]
        ];

        $pagseguro = new PagseguroService();

        return [
            'checkout_code' => $pagseguro->createRequestPayment($data)
        ];
        */
    }

    /*
    public function notify(Request $request){

        try {
            if (Xhr::hasPost()) {

                $pagseguro = new PagSeguro();

                $response = PagseguroNotification::check(
                    $pagseguro->getCredenciais()
                );

                $pagamento = CieloOrder::where('transaction_code', $response->getCode())->first();
                $pagamento->payment_status = $response->getStatus();

                $orderCustomer = Order::find($pagamento->payable->id);

                if ( !$orderCustomer ) {
                    $orderCustomer = WholesaleOrder::find($pagamento->payable->id);
                }

                switch ($pagamento->payment_status) {

                    case 1 :
                        if ( $orderCustomer instanceof Order ) {
                            $orderCustomer->status = 1;
                        } else {
                            $orderCustomer->status = 'pending';
                        }
                        break;

                    case 2 :
                        if ( $orderCustomer instanceof Order ) {
                            $orderCustomer->status = 2;
                        } else {
                            $orderCustomer->status = 'pending';
                        }
                        break;

                    case 3:
                        if ( $orderCustomer instanceof Order ) {
                            $orderCustomer->status = 3;
                            event(new PaymentStatusChange($orderCustomer->customer->user, 'pago'));
                        } else {
                            $orderCustomer->status = 'waiting';
                        }

                        // Aprovado
                        break;

                    case 6:
                    case 7:

                        if ( $orderCustomer instanceof Order ) {
                            $orderCustomer->status = 6;
                            event(new PaymentStatusChange($orderCustomer->customer->user, 'cancelado'));
                        } else {
                            $orderCustomer->status = 'canceled';
                        }
                        // Cancelado
                        break;
                    default:

                        if ( $orderCustomer instanceof Order ) {
                            $orderCustomer->status = 1;
                        } else {
                            $orderCustomer->status = 'new';
                        }

                }

                $orderCustomer->save();
                $pagamento->save();

            } else {
                throw new \Exception('Código invalido');
            }
        } catch (\Exception $e) {

        }

        return $this->responseOk();

    }
    */

    public function  getStatusAttribute($value){
        $value  = $this->desmarcou == 1 ? 'Cancelado' : 'Agendado';
        return strtoupper($value);
    }
}
