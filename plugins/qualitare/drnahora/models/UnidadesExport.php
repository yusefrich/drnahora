<?php namespace Qualitare\Drnahora\Models;

use Model;
use Qualitare\Drnahora\Models\Shosp;

/**
 * Model
 */
class UnidadesExport extends \Backend\Models\ExportModel {

    public function exportData($columns, $sessionKey = null) {
        $subscribers = Unidades::all();
        $subscribers->each(function($subscriber) use ($columns) {
            $subscriber->addVisible($columns);
        });
        return $subscribers->toArray();
    }

}
