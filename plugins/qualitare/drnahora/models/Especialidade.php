<?php namespace Qualitare\Drnahora\Models;

use Model;

/**
 * Especialidade Model
 */
class Especialidade extends Model
{
	/**
	 * @var string The database table used by the model.
	 */
	public $table = 'qualitare_drnahora_especialidades';

	/**
	 * @var array Guarded fields
	 */
	protected $guarded = ['*'];

	/**
	 * @var array Fillable fields
	 */
	protected $fillable = [];

    public $jsonable = ['medicos'];
	public $with = ['medicos'];

	/**
	 * @var array Relations
	 */
	public $hasOne = [];
	public $hasMany = [];
	public $belongsTo = [];
	public $belongsToMany = [
		'medicos' => [
			'Qualitare\Drnahora\Models\Medicos',
			'table' => 'qualitare_drnahora_especialidade_medico'
		],
	];
	public $morphTo = [];
	public $morphOne = [];
	public $morphMany = [];
	public $attachOne = [
		'icone' => ['\System\Models\File']
	];
	public $attachMany = [];

	public function getCodigoShospOptions() {
		$shosp = new Shosp();
		$servicos = [];

		foreach ($shosp->getEspecialidades() as $m) {
			$servicos[$m['codigoEspecialidade']] = $m['nomeEspecialidade'];
		}

		return $servicos;
	}

    public function getMedicosByEspecialidade($esp)
    {
        if (!$esp)
            return [];

        $especialidades = Especialidade::with('medicos')
            ->whereId($esp)->get()->map(function ($item, $index) {
                return $item["medicos"];
            });

        if ($especialidades)
            return $especialidades;

        return [];
    }

}
