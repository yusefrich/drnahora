<?php namespace Qualitare\Drnahora\Models;

use Model;

/**
 * Paciente Model
 */
class Paciente extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_drnahora_pacientes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['user_id', 'nome', 'codigo_shosp'];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'user' => 'RainLab\User\Models\User',
    ];
}
