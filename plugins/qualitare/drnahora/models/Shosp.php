<?php namespace Qualitare\Drnahora\Models;

use GuzzleHttp\Client;

/**
 * Model
 */
class Shosp
{
	private $client;
	private $headers = [
		'Accept' => 'application/json',
		'id' => '',
		'x-api-key' => ''
	];

	public function __construct()
	{
		$this->headers['id'] = env('SHOSP_API_ID');
		$this->headers['x-api-key'] = env('SHOSP_API_KEY');
		$this->client = new Client([
			'base_uri' => env('SHOSP_URL'),
			'verify' => false,
		]);
	}

	public function getMedicos($idEspecialidade = null, $idMedico = null){
		$medicos = [];
		try{
			$response = $this->client->request(
				'GET',
				'/v1/cadastro/prestador',
				[
					'headers' => $this->headers
				]
			);
			$data = json_decode($response->getBody()->getContents(), true);
			if($data['ret'] == 1 && count($data['dados']) > 0){
				foreach($data['dados'] as $m){
					if($m['ativo'] == 1 || $m['cnpj'] == null){
						if($idEspecialidade != null){
							foreach($m['especialidades'] as $espec){
								if($idEspecialidade == $espec['codigoEspecialidade']){
									$medicos[] = $m;
								}
							}
						}elseif($idMedico != null){
							if($m['codigo'] == $idMedico) {
								$medicos[] = $m;
								return $medicos;
							}
						}else{
							$medicos[] = $m;
						}
					}
				}
				return $medicos;
			}
		} catch (\Exception $e) {
			return false;
		}
	}

	public function getServicos(){
		$servicos = array();
		try{
			$response = $this->client->request(
				'GET',
				'/v1/cadastro/servico?tipoServico=C',  //  Shosp vai mudar a API para ser possivel trazer todos os serviços paginados
				[
					'headers' => $this->headers,
					'read_timeout' => 3600,
				]
			);
			$data = json_decode($response->getBody()->getContents(), true);
			if($data['ret'] == 1 && count($data['dados']) > 0){
				return ($data['dados']);
			}
		} catch (\Exception $e) {
		    	    echo $e->getMessage();
		    exit;
			return false;
		}
	}

	public function getExames(){
		$exames = array();
		try{
			$response = $this->client->request(
				'GET',
				'/v1/cadastro/servico',
				[
					'headers' => $this->headers
				]
			);
			$data = json_decode($response->getBody()->getContents(), true);
			if($data['ret'] == 1 && count($data['dados']) > 0){
				foreach($data['dados'] as $s) {
					if($s['tipo'] == "E"){
						$exames[] = $s;
					}
				}
			}
		} catch (\Exception $e) {
			return false;
		}

		return $exames;
	}

	public function getConsultas(){
		$consultas = array();
		try{
			$response = $this->client->request(
				'GET',
				'/v1/cadastro/servico',
				[
					'headers' => $this->headers
				]
			);
			$data = json_decode($response->getBody()->getContents(), true);
			if($data['ret'] == 1 && count($data['dados']) > 0){
				foreach($data['dados'] as $s) {
					if($s['tipo'] == "C"){
						$consultas[] = $s;
					}
				}
			}
		} catch (\Exception $e) {
			return false;
		}
		return $consultas;
	}

	public function getEspecialidades($id = null){
		$especialidades = [];
		try{
			$response = $this->client->request(
				'GET',
				'/v1/cadastro/especialidade',
				[
					'headers' => $this->headers
				]
			);
			$data = json_decode($response->getBody()->getContents(), true);
			if($data['ret'] == 1 && $id == null){
				return $data['dados'];
			}elseif($data['ret'] == 1 && $id != null){
				foreach($data['dados'] as $d){
					if($d['codigoEspecialidade'] == $id){
						$especialidades[] = $d;
					}
				}
				return $especialidades;
			}
		} catch (\Exception $e) {
			return false;
		}
	}

	public function getPaciente($nome){
		try{
			$response = $this->client->request(
				'GET',
				'/v1/cadastro/paciente/nome?nome=' . $nome,
				[
					'headers' => $this->headers
				]
			);
		} catch (\Exception $e) {
			return false;
		}
		return json_decode($response->getBody()->getContents(), true);
	}

	public function createPaciente($name, $email, $phone, $celular = ''){
		$data = [
			'nome' => $name,
			'email' => $email,
			'telefone' => $phone,//str_replace(" ", "", "$phone"),
			'celular' => $celular,//str_replace(" ", "", "$phone"),
			'sexo' => 'M',
			'dataNascimento' => '1990-12-05'
		];

		try{
			$response = $this->client->request(
				'POST',
				'/v1/cadastro/paciente/',
				[
					'headers' => $this->headers,
					'form_params' => $data
				]
			);

		} catch (\Exception $e) {
			throw $e;
		}
		return json_decode($response->getBody()->getContents(), true);
	}

	public function getAgendaByMedico($params = array()){
		$agenda = [];
		try{
			$response = $this->client->request(
				'POST',
				'/v1/agenda/get',
				[
					'headers' => $this->headers,
					'form_params' => $params
				]
			);
			$data = json_decode($response->getBody()->getContents(), true);

			return $data['horarios'][$params['dataInicial']]['horario'];

		} catch (\Exception $e) {
			return false;
		}
	}

	public function getAgenda($start, $limit, $unidade, $especialidade, $medico = null) {
		try {
			$params = [
				'codigoUnidade' => $unidade,
				'dataInicial' => $start,
				'diasMostrar' => $limit
			];

			$params['codigoEspecialidade'] = $especialidade;
			$params['codigoPrestador'] = $medico;

			$response = $this->client->request(
				'POST',
				'/v1/agenda/get',
				[
					'headers' => $this->headers,
					'form_params' => $params
				]
			);

			$data = json_decode($response->getBody()->getContents(), true);
		} catch (\Exception $e) {
			return false;
		}

		return $data;
	}

	public function getUnidades($id = null){
		$unidades = [];
		try{
			$response = $this->client->request(
				'GET',
				'/v1/cadastro/unidade',
				[
					'headers' => $this->headers
				]
			);
			$data = json_decode($response->getBody()->getContents(), true);
			if($data['ret'] == 1 && $id == null){
				return $data['dados'];
			}
		} catch (\Exception $e) {
			return false;
		}
	}

    public function createAgendamento($data)
    {
        try{
            $response = $this->client->request(
                'POST',
                '/v1/agenda',
                [
                    'headers' => $this->headers,
                    'form_params' => $data
                ]
            );
        } catch (\Exception $e) {
            return false;
        }
        $return = json_decode($response->getBody()->getContents(), true);

        if ($return['ret'] == 1){
            return $return['dados'];
        }{
            return false;
        }
    }

    public function cancelarAgendamento($codigo_shosp) {

        $unidades = [];
        try{

            $data = [
                'codigoAgendamento' => $codigo_shosp
            ];

            $response = $this->client->request(
                'POST',
                '/v1/agenda/cancelaragendamento',
                [
                    'headers' => $this->headers,
                    'form_params' => $data
                ]
            );
            $data = json_decode($response->getBody()->getContents(), true);
            if($data['ret'] == 1){
                return $data['dados'];
            }
        } catch (\Exception $e) {
            return false;
        }

    }

}
