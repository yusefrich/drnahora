<?php

Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'clientes'], function () {
        Route::post('new', ['uses' => 'Qualitare\Drnahora\Controllers\Cliente@create']);
    });
    Route::get('especialities', ['uses' => 'Qualitare\Drnahora\Controllers\Especialidades@especialities']);
    Route::get('schedules/user', ['uses' => 'Qualitare\Drnahora\Controllers\Agendamentos@user']);
    Route::get('schedules/{especiality}', ['uses' => 'Qualitare\Drnahora\Controllers\Agendamentos@schedules']);
    Route::delete('schedules/delete', ['uses' => 'Qualitare\Drnahora\Controllers\Agendamentos@destroy']);
    Route::post('schedules', ['uses' => 'Qualitare\Drnahora\Controllers\Agendamentos@store']);

    Route::post('pagseguro', ['uses' => 'Qualitare\Drnahora\Controllers\Agendamentos@checkout']);

    Route::post('notify', ['uses' => 'Qualitare\Drnahora\Controllers\Agendamentos@notify']);

    Route::group(['prefix' => 'reset-password'], function () {
        Route::post('/', ['uses' => 'Gogo\Account\Controllers\Auth@reset']);
        Route::name('reset.password')->get('/{token}', ['uses' => 'Gogo\Account\Controllers\Auth@resetView']);
        Route::post('/{token}', ['uses' => 'Gogo\Account\Controllers\Auth@resetPassword']);
    });
});

Route::get('/sitemap', ['uses' => 'Qualitare\Drnahora\Controllers\SitemapController@index']);

Route::get('/auth/redirect/{provider}', 'Qualitare\Drnahora\Controllers\SocialController@redirect');
Route::get('/auth/{provider}/callback', 'Qualitare\Drnahora\Controllers\SocialController@callback');

//Route::post('sociallogin/{provider}', 'Auth\AuthController@SocialSignup');
//Route::get('/auth/{provider}/callback', 'Qualitare\Drnahora\Controllers\SocialController@index')->where('provider', '.*');
