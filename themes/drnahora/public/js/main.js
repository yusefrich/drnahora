(function() {

    new Vue({
        delimiters: ['<%','%>'],
        render (h) {
            return h('#vue-container', this.hi)
        },
        data: () => ({
            consultas: $(".json").data('json')
        })
    })
  if($('.searchbar-form-input').length > 0) {



    $('.gulpsearchbar-form-input').on('blur', function() {
      setTimeout(function() {
        $(this).parent().find('.searchbar-form-auto').removeClass('active');

      }, 500);
    });


    $('.searchbar-form-input').on('keyup', function() {
      var input  = $(this),
          auto   = $(this).parent().find('.searchbar-form-auto'),
          length = input.val().length;

      if(length >= 3) {
        var availableTags = $(".json").data('json');
        console.log()

            auto.addClass('active');
        } else {
            auto.removeClass('active');
        }

      auto.find('li').on('click', function() {
        input.val($(this).text());
        auto.removeClass('active');
      });
    });
  }

  var animateHeaderMenuBorder = function() {
    // Cria a posição inicial da borda animal

    $('.header-menu-list-border').css({
      width: $('.header-menu-list-item.active').length > 0 ? $('.header-menu-list-item.active')[0].clientWidth : 0,
      left: $('.header-menu-list-item.active').length > 0  ? $('.header-menu-list-item.active')[0].offsetLeft : 0
    });

    // Move a borda animada pelo menu
    $('.header-menu-list-item').on('mouseenter', function() {
      $('.header-menu-list-item.active .header-menu-list-item-border').animate({
        opacity: 0
      }, 250);

      $('.header-menu-list-border').animate({
        opacity: 1,
        width: $(this)[0].clientWidth,
        left: $(this)[0].offsetLeft
      }, 250);
    });

    // Retorna a borda animada a sua posição inicial
    $('.header-menu-list').on('mouseleave', function() {
      $('.header-menu-list-border').animate({
        width: $('.header-menu-list-item.active').length > 0 ? $('.header-menu-list-item.active')[0].clientWidth : 0,
        left: $('.header-menu-list-item.active').length > 0 ? $('.header-menu-list-item.active')[0].offsetLeft : 0
      }, 250, 'swing', function() {
        $('.header-menu-list-item.active .header-menu-list-item-border').animate({
          opacity: 1
        }, 250);
      });
    });
  };

  animateHeaderMenuBorder();

  var testimonialsSlides = function() {
    $('.testimonials-slides').slick({
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      centerMode: true,
      centerPadding: '300px',
      responsive: [{
        breakpoint: 1200,
        settings: {
          centerPadding: '200px',
        }
      }, {
        breakpoint: 992,
        settings: {
          centerPadding: '150px',
        }
      }, {
        breakpoint: 768,
        settings: {
          centerPadding: '100px',
        }
      }, {
        breakpoint: 576,
        settings: {
          centerPadding: '25px',
        }
      }]
    });
  };

  if($('.testimonials-slides').length > 0) {
    testimonialsSlides();
  }

  var locationGallery = function() {
    var currentSlide = 0,
        totalSlides  = $('.location-slides-item').length,
        nextImage    = $('.location-slides-item.active').next().find('img').attr('src');

    function loadNextImage() {
      $('.location-slides-nav').removeClass('active');

      var url = $('.location-slides-item.active').next().find('img').attr('src');
      
      if(url == undefined) {
        nextImage = $($('.location-slides-item')[0]).find('img').attr('src');;
      } else {
        nextImage = url;
      }
      
      $('.location-slides-nav').css('background-image', "url('/themes/drnahora/public/images/bg-location.png'), url('" + nextImage + "')");
    }

    function next() {
      $('.location-slides-nav').on('mouseenter', function(e) {
        $(this).addClass('active');
      });

      $('.location-slides-nav').on('mouseleave', function(e) {
        $(this).removeClass('active');
      });

      $('.location-slides-nav').on('click', function(e) {
        if(currentSlide < (totalSlides - 1)) {
          currentSlide += 1;
        } else {
          if(currentSlide == totalSlides - 1) {
            currentSlide = 0;
          }
        }

        $('.location-maps-item.active').removeClass('active')
        $($('.location-maps-item')[currentSlide]).addClass('active')
        
        $('.location-places-item.active').removeClass('active')
        $($('.location-places-item')[currentSlide]).addClass('active')

        $('.location-slides-item.active').removeClass('active');
        $($('.location-slides-item')[currentSlide]).addClass('active');
        
        loadNextImage();
      });
    }

    next();

    loadNextImage();
  };

  if($('.location-slides').length > 0) {
    locationGallery();
  }

  var dropdownMenus = function() {
    $('.global-dropdown-list').each(function() {
      $(this).on('mouseenter', function() {
        $(this).addClass('active');
      });

      $(this).on('mouseleave', function() {
        $(this).removeClass('active');
      });
    });
  };

  if($('.global-dropdown-list').length > 0) {
    dropdownMenus();
  }

  var medicosCategories = function() {
    $('.medicos-category-list').slick({
      slidesToShow: 7,
      infinite: false,
      nextArrow: '.medicos-category-list-arrow.next',
      prevArrow: '.medicos-category-list-arrow.prev'
    });
  };

  if($('.medicos-category-list').length > 0) {
    medicosCategories();
  }

  var inscricaoSlides = function() {
    $('.inscricao-testimonials-slides-items').slick({
      fade: true,
      infinite: false,
      dots: true,
      appendDots: '.inscricao-testimonials-slides-dots',
      prevArrow: '.inscricao-testimonials-slides-nav.prev',
      nextArrow: '.inscricao-testimonials-slides-nav.next'
    });
  };

  if($('.inscricao-testimonials-slides-items').length > 0) {
    inscricaoSlides();
  }

  var blogGallery = function() {
    var currentSlide = 0,
        totalSlides  = $('.blog-slides-item').length,
        nextImage    = $('.blog-slides-item.active').next().find('img').attr('src');

    function loadNextImage() {
      $('.blog-slides-nav').removeClass('active');

      var url = $('.blog-slides-item.active').next().find('img').attr('src');
      
      if(url == undefined) {
        nextImage = $($('.blog-slides-item')[0]).find('img').attr('src');;
      } else {
        nextImage = url;
      }
      
      $('.blog-slides-nav').css('background-image', "url('/themes/drnahora/public/images/bg-location.png'), url('" + nextImage + "')");
    }

    function calculeNext() {
      if(currentSlide < (totalSlides - 1)) {
        currentSlide += 1;
      } else {
        if(currentSlide == totalSlides - 1) {
          currentSlide = 0;
        }
      }

      $('.blog-slides-item.active').removeClass('active');
      $($('.blog-slides-item')[currentSlide]).addClass('active');

      $('.blog-slides-dots li.active').removeClass('active');
      $($('.blog-slides-dots li')[currentSlide]).addClass('active');
    }

    function next() {
      $('.blog-slides-nav').on('mouseenter', function(e) {
        $(this).addClass('active');
      });

      $('.blog-slides-nav').on('mouseleave', function(e) {
        $(this).removeClass('active');
      });

      $('.blog-slides-dots li').on('click', function(e) {
        if($(e.target).index() != currentSlide) {
          calculeNext();
        }
        
        loadNextImage();
      });

      $('.blog-slides-nav').on('click', function(e) {
        calculeNext();
        
        loadNextImage();
      });
    }

    next();

    loadNextImage();
  };

  if($('.blog-slides').length > 0) {
    blogGallery();
  }

  var contatoMaps = function() {
    $('.maps-panel-list a').on('click', function(e) {
      e.preventDefault();

      var city    = $(e.target).html();
      var target  = $(e.target).data('target');
      var address = $(e.target).data('address');
      var phone   = $(e.target).data('phone');

      $('.dropdown-list-item.selected span').html(city);

      $('.maps-panel-list').removeClass('active');

      $('.maps-panel-address').html(address);
      $('.maps-panel-phone span').html(phone);

      $('.contato-maps-item').removeClass('active')
      $('.contato-maps-item[data-target=' + target + ']').addClass('active')
    });
  };

  if($('.contato-maps').length > 0) {
    contatoMaps();
  }

  $("#video-modal").on("show.bs.modal", function(e) {
    $("#modal-video-embed").attr("src", "https://www.youtube.com/embed/" + $(e.relatedTarget).data("video-id") + "?rel=0");
  });

  $("#video-modal").on("hide.bs.modal", function(e) {
    $("#modal-video-embed").attr("src", "");
  });

  $('.schedule-form-select').each(function() {
    var select = $(this),
        label  = select.find('.schedule-form-select-selected .labe'),
        hidden = select.find('input[type=hidden]');

    select.on('click', function() {
      select.toggleClass('active');
    });

    select.find('li').each(function() {
      var item = $(this);

      item.on('click', function() {
        var html  = $(this).data('label'),
            value = $(this).data('value');

        label.html(html);
        hidden.val(value);
      });
    });
  });

  $('.schedule-form-hours').each(function() {
    var item = $(this);

    item.find('.schedule-form-hours-header').on('click', function() {
      $('.schedule-form-hours').removeClass('active');
      
      item.addClass('active');
    });

    item.find('li a').on('click', function(e) {
      e.preventDefault();
      e.stopPropagation();

      var li     = $(this).parent(),
          medico = li.data('medico'),
          hora   = li.data('hora');

      item.find('li').removeClass('active');
      li.addClass('active');

      $('input[name=medico]').val(medico);
      $('input[name=hora]').val(hora);
    });
  });

  $('.schedule-form-radio').each(function() {
    var item  = $(this),
        value = item.data('value');
    
    item.on("click", function() {
      $('.schedule-form-radio').removeClass('active');
      item.addClass('active');

      $('input[name=pagamento]').val(value);
    });
  });

  $('input[type=file]').each(function() {
    var input = $(this);

    input.on('change', function(e) {
      values = e.target.value.split("\\");

      $(this).parent().find('span').html(values[values.length - 1]);
    });
  });


  $('.header-menu-button').on('click', function() {
    $('.header-menu').toggleClass('active');
  });

  $('.footer-contact-list').on('click', function() {
    $(this).toggleClass('active');
  });

  $('.footer-contact-list .dropdown-sublist-item').each(function() {
    var item   = $(this),
        target = item.data('target'),
        city   = item.text();

    item.on('click', function() {
      $('.footer-contact-phone').removeClass('active');
      $('.' + target).addClass('active');

      $('.footer-contact-list .dropdown-list-item.selected').find('span').html(city);
    });
  });


  if($('#success-modal').length > 0) {
    if(window.location.search.includes("showModal=1")) {
      $('#success-modal').modal();
    }
  }

  if($('.countdown').length > 0) {
    var flag = false;
    
    $(document).on('scroll', function(e) {
      var offsetTop = $('.cardinfo-list').offset().top;

      if($(this).scrollTop() >= (offsetTop - 200) && flag == false) {
        flag = true;

        $('.countdown').each(function() {
          var count = $(this),
              start = 0,
              end   = parseInt($(this).data('end'));

          var counter = function() {
            if(start < end) {
              start += 1;
              count.html(start);
              
              setTimeout(counter, 10);
            }
          };

          counter();


        });
      }
    });
  }

  if($('.parallax-items').length > 0) {

    $('.parallax-items img').each(function() {
      var item = $(this),
          top  = item.offset().top,
          pTop = parseInt(item.css('top').replace('px', '')),
          cTop = pTop,
          lTop = 0;

      $(document).on('scroll', function() {
        var scrollTop = $(this).scrollTop();

        if(scrollTop > lTop) {
          pTop -= 0.5;
          item.css('top', pTop);
        } else {
          pTop += 0.5;
          item.css('top', pTop);
        }

        lTop = scrollTop;
      });

    });

  }










})();

var loadMaps = function() {
  var pos1 = {lat: -7.214437, lng: -35.88397}, 
      map1 = new google.maps.Map(document.getElementById('map-unit-1'), {
        center: pos1,
        zoom: 16
      })
      marker = new google.maps.Marker({position: pos1, map: map1});

  var pos2 = {lat: -7.1562623, lng: -34.8410523}, 
      map2 = new google.maps.Map(document.getElementById('map-unit-2'), {
        center: pos2,
        zoom: 16
      })
      marker = new google.maps.Marker({position: pos2, map: map2});
};


















