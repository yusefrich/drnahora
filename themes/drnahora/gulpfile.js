var gulp = require('gulp'),
	path = require('path'),
	webserver = require('gulp-webserver'),
	sass = require('gulp-sass'),
	cssmin = require('gulp-cssmin'),
	webpack = require('webpack'),
	webpackStream = require('webpack-stream'),
	imagemin = require('gulp-imagemin'),
	spritesmith = require('gulp.spritesmith'),
	buffer = require('vinyl-buffer'),
	merge = require('merge-stream'),
	sourcemaps = require('gulp-sourcemaps'),
	gulpResolveUrl = require('gulp-resolve-url'),
	babel = require('gulp-babel'),
	VueLoader = require('vue-loader');

var ROOT_PATH = './themes/drnahora/',
	SASS_PATH = 'app/scss/**/*.scss',
	CSS_PATH = './public/css',
	JS_APP_PATH = 'app/js/**/*.js',
	JS_PATH = 'public/js',
	IMAGES_APP_PATH = 'app/images/*',
	IMAGES_PATH = 'public/images',
	SPRITES_PATH = 'app/sprites/*.png';

gulp.task('webserver', function () {
	gulp.src(ROOT_PATH)
		.pipe(webserver({
			livereload: true
		}));
});

gulp.task('sass', function () {
	gulp.src(SASS_PATH)
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(gulpResolveUrl())
		.pipe(gulp.dest(CSS_PATH));
});

gulp.task('cssmin', function () {
	gulp.src(CSS_PATH + '/**/*.css')
		.pipe(cssmin())
		.pipe(gulp.dest(CSS_PATH));
});

gulp.task('build', function () {
	gulp.src('app/main.js')
		.pipe(webpackStream({
			devtool: 'cheap-module-source-map',
			entry: [
				'./app/main.js'
			],
			output: {
				filename: 'main.bundle.js'
			},
			plugins: [
				new VueLoader.VueLoaderPlugin(),
				new webpack.DefinePlugin({
					'process.env': {
						'MAPS_KEY': JSON.stringify('AIzaSyBpDasTQL39guurRUq5gsNalfNFzFNRXPU')
					}
				}),
				new webpack.LoaderOptionsPlugin({
					minimize: true,
					debug: false
				})
			],
			resolve: {
				extensions: ['.js', '.scss'],
				alias: {
					'@': path.join(__dirname, 'app'),
					'~': path.join(__dirname, 'node_modules')
				}
			},
			module: {
				rules: [
					{
						test: /\.vue$/,
						exclude: '/node_modules',
						use: [
							'vue-loader',
						],
					},
					{
						test: /\.scss$/,
						use: [
							'vue-style-loader',
							'css-loader',
							{
								loader: 'sass-loader',
								options: {
									data: "@import '~@/stylesheets/core.scss';",
									includePaths: [
										require('bourbon-neat').includePaths,
									]
								}
							}
						]
					},
					{
						test: /\.m?js$/,
						exclude: /(node_modules|bower_components)/,
						use: {
							loader: 'babel-loader',
							options: {
								presets: ['@babel/preset-env']
							}
						}
					}
				]
			}
		}), webpack)
		.pipe(gulp.dest(JS_PATH));
});

gulp.task('scripts', function () {
	gulp.src('app/main.js')
		.pipe(webpackStream({
			mode: 'development',
			watch: true,
			entry: [
				'./app/main.js'
			],
			output: {
				filename: 'main.bundle.js'
			},
			plugins: [
				new VueLoader.VueLoaderPlugin(),
				new webpack.DefinePlugin({
					'process.env': {
						'MAPS_KEY': JSON.stringify('AIzaSyBpDasTQL39guurRUq5gsNalfNFzFNRXPU')
					}
				})
			],
			resolve: {
				extensions: ['.js', '.scss'],
				alias: {
					'@': path.join(__dirname, 'app'),
					'~': path.join(__dirname, 'node_modules')
				}
			},
			module: {
				rules: [
					{
						test: /\.vue$/,
						exclude: '/node_modules',
						use: [
							'vue-loader',
						],
					},
					{
						test: /\.scss$/,
						use: [
							'vue-style-loader',
							'css-loader',
							{
								loader: 'sass-loader',
								options: {
									data: "@import '~@/stylesheets/core.scss';",
									includePaths: [
										require('bourbon-neat').includePaths,
									]
								}
							}
						]
					},
					{
						test: /\.m?js$/,
						exclude: /(node_modules|bower_components)/,
						use: {
							loader: 'babel-loader',
							options: {
								presets: ['@babel/preset-env']
							}
						}
					}
				]
			}
		}), webpack)
		.pipe(gulp.dest(JS_PATH));
});

gulp.task('imagemin', function () {
	gulp.src(IMAGES_APP_PATH)
		.pipe(imagemin())
		.pipe(gulp.dest(IMAGES_PATH));
});

gulp.task('spritesmith', function () {
	var data = gulp.src(SPRITES_PATH)
		.pipe(spritesmith({
			imgName: 'sprites.png',
			imgPath: '/' + IMAGES_PATH + '/sprites.png',
			cssName: '_sprites.scss'
		}));

	var imgStream = data.img
		.pipe(buffer())
		.pipe(imagemin())
		.pipe(gulp.dest(IMAGES_PATH));

	var cssStream = data.css
		.pipe(gulp.dest('app/scss'));

	return merge(imgStream, cssStream);
});

gulp.task('default', gulp.series('scripts'), function () {
	gulp.watch(SASS_PATH, ['sass']);
	gulp.watch(CSS_PATH + '/**/*.css', ['cssmin']);
	gulp.watch(IMAGES_APP_PATH, ['imagemin']);
	gulp.watch(SPRITES_PATH, ['spritesmith']);
});
