export default {
	bind(el, binding, vnode) {
		function onResize(event) {
			el.style.height = window.innerHeight + 'px'
		}

		onResize()
		window.addEventListener('resize', onResize)

		el.$destroy = () => {
			window.removeEventListener('resize', onResize)
		}
	},

	unbind(el, binding, vnode) {
		el.$destroy()
	}
}
