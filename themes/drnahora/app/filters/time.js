import moment from 'moment'

export default function (value) {
	return moment(value, 'HH:mm:ss').format('HH:mm')
}
