import Vue from 'vue/dist/vue'
import Vuex from 'vuex'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import VCalendar from 'v-calendar'
import loadGoogleMapsApi from 'load-google-maps-api'
import VueMeta from 'vue-meta'
import NProgress from 'nprogress'
import { TweenMax, Power2 } from 'gsap/TweenMax'
import VeeValidate from 'vee-validate'

import VueAxios from 'vue-axios'
import VueSocialauth from 'vue-social-auth'
import axios from 'axios'

// Views
import DRWelcome from './components/pages/DRWelcome.vue'
import DRApp from './DRApp.vue'
import DRHome from './components/pages/DRHome.vue'
import DRBlog from './components/pages/DRBlog.vue'
import DRPost from './components/pages/DRPost.vue'
import DRAuth from './components/pages/DRAuth.vue'
import DRPlayground from './components/templates/DRPlayground.vue'
import DRConsultas from './components/pages/DRConsultas.vue'
import DRConsulta from './components/pages/DRConsulta.vue'
import DRExames from './components/pages/DRExames.vue'
import DRExame from './components/pages/DRExame.vue'
import DRInscricao from './components/pages/DRInscricao.vue'
import DRContato from './components/pages/DRContato.vue'
import DRMedicos from './components/pages/DRMedicos.vue'
import DRMedico from './components/pages/DRMedico.vue'
import DRCategorias from './components/pages/DRCategorias.vue'
import DRLogin from './components/organisms/DRLogin.vue'
import DRRestore from './components/organisms/DRRestore.vue'
import DRLoginReset from './components/organisms/DRLoginReset.vue'
import DRLoginResetSuccess from './components/organisms/DRLoginResetSuccess.vue'
import DRRegister from './components/organisms/DRRegister.vue'
import DRAppointmentScheduled from './components/pages/DRAppointmentScheduled.vue'
import DREditUser from './components/pages/DREditUser.vue'
import DRAppointmentPending from './components/pages/DRAppointmentPending.vue'
import DRAppointmentHistory from './components/pages/DRAppointmentHistory.vue'
import DRSchedule from './components/pages/DRSchedule.vue'
import DRSocialLogin from "./components/organisms/DRSocialLogin.vue"

// Styles
import './stylesheets/defaults'

export const router = new VueRouter({
	routes: [
		{
			path: '/',
			name: 'home',
			component: DRHome
		},
		{
			path: '/blog',
			name: 'blog',
			component: DRBlog
		},
		{
			path: '/post/:id',
			name: 'post',
			component: DRPost
		},
		{
			path: '/playground',
			name: 'playground',
			component: DRPlayground
		},
		{
			path: '/consultas',
			name: 'consultas',
			component: DRConsultas
		},
		{
			path: '/consulta/:id',
			name: 'consulta',
			component: DRConsulta
		},
		{
			path: '/exames',
			name: 'exames',
			component: DRExames
		},
		{
			path: '/exame/:id',
			name: 'exame',
			component: DRExame
		},
		{
			path: '/categorias/:slug',
			name: 'categorias',
			component: DRCategorias
		},
		{
			path: '/inscricao',
			name: 'inscricao',
			component: DRInscricao
		},
		{
			path: '/contato',
			name: 'contato',
			component: DRContato
		},
		{
			path: '/medicos',
			name: 'medicos',
			component: DRMedicos
		},
		{
			path: '/medico/:id',
			name: 'medico',
			component: DRMedico
		},
		{
			path: '/schedule/:servico_id/:type/:medico_id?',
			name: 'schedule',
			component: DRSchedule
		},
		{
			path: '/auth',
			component: DRAuth,
			children: [
				{
					path: '',
					redirect: '/auth/login'
				},
				{
					path: 'login',
					name: 'auth-login',
					component: DRLogin
				},
				{
					path: 'reset',
					name: 'auth-reset',
					component: DRLoginReset
				},
				{
					path: 'restore/:id/:code',
					name: 'auth-restore',
					component: DRRestore
				},
				{
					path: 'reset/success',
					name: 'auth-reset-success',
					component: DRLoginResetSuccess
				},
				{
					path: 'register',
					name: 'auth-register',
					component: DRRegister
				},
                {
                    path: ':provider/callback',
                    component: DRSocialLogin
                    // component: {
                    //     template: '<div class="auth-component"></div>'
                    // }
                }
			]
		},
		{
			path: '/dashboard',
			name: 'dashboard',
			component: DRWelcome,
			children: [
				{
					path: '',
					redirect: '/dashboard/edit'
				},
				{
					path: 'edit',
					name: 'dashboard-edit',
					component: DREditUser
				},
				{
					path: 'scheduled',
					name: 'dashboard-scheduled',
					component: DRAppointmentScheduled
				},
				{
					path: 'pending',
					name: 'dashboard-pending',
					component: DRAppointmentPending
				},
				{
					path: 'history',
					name: 'dashboard-history',
					component: DRAppointmentHistory
				},
			]
		}
	],
	scrollBehavior(to, from, savedPosition) {
		return new Promise((resolve, reject) => {
			TweenMax.to(document.scrollingElement, .5, {
				scrollTop: 0,
				ease: Power2.easeOut,
				onComplete() {
					return resolve({ x: 0, y: 0 })
				}
			})
		})
	}
})

Vue.use(Vuex);
Vue.use(VeeValidate);
Vue.use(VueAxios, axios)
Vue.use(VueSocialauth, {
    providers: {
        facebook: {
            clientId: process.env.FACEBOOK_KEY,
            redirectUri: process.env.FACEBOOK_REDIRECT_URI
        },
        google: {
            clientId: process.env.GOOGLE_CLIENT_ID,
            redirectUri: process.env.GOOGLE_CALLBACK_URL
        }
    }
})

export const store = new Vuex.Store({
	state: {
		user: null
	},
	getters: {
		getUser(state) {
			return state.user
		}
	},
	actions: {
		fetchUser({ commit }, payload) {
		    return new Promise(resolve => {
				$.request('onGetUser', {
					success: user => {
						commit('setUser', user)
						resolve(user)
					}
				})
			})
		},

		logout({ commit }, payload) {
			return new Promise(resolve => {
				$.request('onLogout', {
					success: () => {
						commit('setUser', null)
						resolve()
					}
				})
			})
		}
	},
	mutations: {
		setUser(state, value) {
		    console.log('setUser', value)
			state.user = value
		}
	},
})

if (document.getElementById('dr-app')) {
	Vue.use(VueRouter)
	Vue.use(VueMeta)

	// Setup v-calendar
	Vue.use(VCalendar, {
		firstDayOfWeek: 2,
		popoverVisibility: 'focus'
	});

	$(document).on('ajaxSetup', NProgress.start)
	$(document).on('ajaxDone', NProgress.done)
	$(document).on('ajaxFail', NProgress.done)

	new Vue({
		el: '#dr-app',
		router: router,
		store: store,
		render: h => h(DRApp),
		mounted() {
			// Initialize Google Maps
			loadGoogleMapsApi({
				key: process.env.MAPS_KEY
			})
				.then(maps => {
					this.$root.$emit('maps-init', maps)
				})
		}
	})
}

// =========================================================

if (document.getElementById('vue-search')) {
    new Vue({
        delimiters: ['<%', '%>'],
        el: '#vue-search',
        data: () => ({
            data: [],
            posts: [],
            especialidades: [],
            autocomplete: [],
            features: [],
            response: [],
            filtered: [],
            q: '',
            limit: 6,
            page: 1
        }),
        mounted() {
            $.request('onLoadData', {
                success: (data) => {
                    this.response = data.slice(0, (this.limit * this.page))
                }
            });
            $.request('onLoadMedicos', {
                success: (data) => {
										this.response = data;
										this.data = data

                    setTimeout( () => {
                        if($('.medicos-category-list').length > 0) {
                            $('.medicos-category-list').slick({
                                slidesToShow: 7,
                                infinite: false,
                                nextArrow: '.medicos-category-list-arrow.next',
                                prevArrow: '.medicos-category-list-arrow.prev'
                            });
                        }
                    }, 1000)

                }
            });
            $.request('onLoadEspecialidades', {
                success: (data) => {
                    this.especialidades = Object.values(data)
                }
            })
        },
        methods: {
            viewMore(event){
                this.page++;
                this.response = this.data.slice(0, (this.limit * this.page));
                event.preventDefault()
            },
            search(event) {
                if (this.q == '') {
                    return this.response = this.data.slice(0, (this.limit * this.page))
                } else {
                    this.response = this.data.filter(value => {
                        return value.name.toLowerCase().includes(this.q.toLowerCase()) == true
                    }).slice(0, (this.limit * this.page));
                }
            },
            getEspecialidades(event){
						  this.filtered = this.especialidades.filter(value => {
								return value.nome.toLowerCase().includes(this.q.toLowerCase()) == true
							});
            }
        }
    })
}


(function() {
	var animateHeaderMenuBorder = function() {
		// Cria a posição inicial da borda animal

		$('.header-menu-list-border').css({
			width: $('.header-menu-list-item.active').length > 0 ? $('.header-menu-list-item.active')[0].clientWidth : 0,
			left: $('.header-menu-list-item.active').length > 0  ? $('.header-menu-list-item.active')[0].offsetLeft : 0
		});

		// Move a borda animada pelo menu
		$('.header-menu-list-item').on('mouseenter', function() {
			$('.header-menu-list-item.active .header-menu-list-item-border').animate({
				opacity: 0
			}, 250);

			$('.header-menu-list-border').animate({
				opacity: 1,
				width: $(this)[0].clientWidth,
				left: $(this)[0].offsetLeft
			}, 250);
		});

		// Retorna a borda animada a sua posição inicial
		$('.header-menu-list').on('mouseleave', function() {
			$('.header-menu-list-border').animate({
				width: $('.header-menu-list-item.active').length > 0 ? $('.header-menu-list-item.active')[0].clientWidth : 0,
				left: $('.header-menu-list-item.active').length > 0 ? $('.header-menu-list-item.active')[0].offsetLeft : 0
			}, 250, 'swing', function() {
				$('.header-menu-list-item.active .header-menu-list-item-border').animate({
					opacity: 1
				}, 250);
			});
		});
	};

	animateHeaderMenuBorder();

	var dropdownMenus = function() {
		$('.global-dropdown-list').each(function() {
			$(this).on('mouseenter', function() {
				$(this).addClass('active');
			});

			$(this).on('mouseleave', function() {
				$(this).removeClass('active');
			});
		});
	};

	/*if($('.global-dropdown-list').length > 0) {
		dropdownMenus();
	}

	$('.footer-contact-list').on('click', function() {
		$(this).toggleClass('active');
	});

	$('.footer-contact-list .dropdown-sublist-item').each(function() {
		var item   = $(this),
			target = item.data('target'),
			city   = item.text();

		item.on('click', function() {
			$('.footer-contact-phone').removeClass('active');
			$('.' + target).addClass('active');
			$('.footer-contact-list .dropdown-list-item.selected').find('span').html(city);
		});
	});*/

	if($('.parallax-items').length > 0) {

		$('.parallax-items img').each(function() {
			var item = $(this),
				top  = item.offset().top,
				pTop = parseInt(item.css('top').replace('px', '')),
				cTop = pTop,
				lTop = 0;

			$(document).on('scroll', function() {
				var scrollTop = $(this).scrollTop();

				if(scrollTop > lTop) {
					pTop -= 0.5;
					item.css('top', pTop);
				} else {
					pTop += 0.5;
					item.css('top', pTop);
				}

				lTop = scrollTop;
			});

		});

	}
})();

$(document).ready(function(){
    $("#whatsapp-bloco").hide();
    setTimeout(function(){
        $("#whatsapp-bloco").show();
    }, 3000);
});
